pv image, for monitoring the progress of data through a pipe.

Packages the `pv` executable, which is also the entrypoint of this container.

Volumes: None

Documentation:
For this container, see https://gitlab.com/c8160/btrbk

Requirements: None

Configuration: None

