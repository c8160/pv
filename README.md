# pv

Monitor the progress of data through a pipe.

> This image is tested for use with **podman** on **Linux** hosts with
> **x86_64** arch


## What is pv?

pv allows a user to see the progress of data through a pipeline, by giving
information such as time elapsed, percentage completed (with progress bar),
current throughput rate, total data transferred, and ETA.


## How to use this image

This image is meant to be a one-to-one replacement of a natively installed
`pv`. Instead of calling `pv`, you can run it from this container via:

```bash
$ podman run --rm -i --log-driver none registry.gitlab.com/c8160/pv:latest
```

It is important to pass the `--log-driver none` option since otherwise podman
will copy all contents the container writes to `stdout` (i.e. the pipe you are
monitoring) into the systemd journal by default.

Note that due to the way containers work, `pv` isn't attached to a real TTY.
Hence, the `-f` CLI argument is always active by default to force `pv` to
display progress information on stderr unconditionally. If you do not want this
behavior, you can temporarily override it with:

```bash
$ podman run --rm -i --log-driver none --entrypoint /usr/bin/pv registry.gitlab.com/c8160/pv:latest
```


## Limitations

The invocation shown above doesn't grant the container filesystem access.
Hence, calling `pv` with files as arguments to read is *not* supported. If you
need this functionality, you can add a volume mount to the container like this:

```bash
podman run --rm -i --log-driver none -v "$PWD:/$PWD:z" -w "$PWD" registry.gitlab.com/c8160/pv:latest
```


## Getting help

First you may want to refer to the `help.md` contained in this repository or
the containers rootfs. If you're still stuck or found an issue with the
container in particular, feel free to open an issue on Gitlab:
https://gitlab.com/c8160/pv/-/issues

